# Spite

Game library and such inspired by Love2D built on top of [r7rs-pffi](https://codeberg.org/r7rs-pffi).

## Getting Started

### Hello World

Easiest way to get started is with
[spite-hello](https://codeberg.org/Spite/spite-hello).

For windows it contains everything you need, for linux requirements read ahead.

If you want to install spite manually read untill the end.

### System requirements <a name="system-requirements"></a>

#### Linux

##### Sagittarius

On Debian and derivatives (Ubuntu/Mint...) download the latest source from
[https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/](https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/)
unpack it and cd into it, then run

<code>
  apt install cmake libgc-dev zlib1g-dev libffi-dev libssl-dev
  mkdir build
  cd build
  cmake ..
  make
  make install
</code>

##### SDL2

On linux install sdl2 sdl2-image sdl2-ttf sdl2-mixer.

On Debian and derivatives (Ubuntu/Mint...) run

<code>
  apt install libsdl2-2.0-0 libsdl2-image-2.0-0 libsdl2-ttf-2.0-0 libsdl2-mixer-2.0-0
</code>

#### Windows

##### Sagittarius

You can download the installer from here
[https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/](https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/)

##### SDL2

All the needed DLL files are included with Spite.


### Installing

1. Install [Schubert](https://codeberg.org/Schubert)
2. Run "schubert init" in your projects folder
3. Add (retropikzel spite VERSION) into composition.scm dependencies section
4. Run "schubert compose"

Spite is now installed into projects "schubert" folder.

### Manual hello world

Create main.scm with this code

<pre>
<code>
(import (scheme base)
        (scheme write)
        (retropikzel spite v0.1.0 main))
  (spite-load
    (lambda ()
      (display "Loading...")
      (newline)))
  (spite-update
    (lambda ()
      (display "Updating...")
      (newline)))
  (spite-draw
    (lambda ()
      (display "Drawing...")
      (newline)))
  (spite-event-handle
    'key-down
    (lambda (key scancode repeat?)
      (if (string=? key "Space")
      (spite-event-push 'test (list "I am user defined event")))))
  (spite-event-handle
    'test
    (lambda (message)
      (display message)
      (newline)))
  (spite-start "Game Test" 400 400)
</code>
</pre>

Run with either

<code>sash -r7 -L . -L ./schubert main.scm</code>

or

<code>sash.exe -r7 -L . -L ./schubert main.scm</code>


## Procedures

### spite-load <a name="spite-load"></a>

Arguments:

- proc (procedure)
  - Procedure to run before the game starts

Example:

(spite-load (lambda () (display "Loading...") (newline)))


### spite-update <a name="spite-update"></a>

Arguments:

- proc (procedure)
  - Procedure to run on the games main loop before drawing

Example:

(spite-update (lambda () (display "Updating...") (newline))


### spite-draw <a name="spite-draw"></a>

Arguments:

- proc (procedure)
  - Procedure inside which all games drawing should happen

Example:

(spite-draw (lambda () (display "Drawing...") (newline)))


### spite-image-load <a name="spite-image-load"></a>

Arguments:

- path (string)
  - Path to the image you want to load

Returns: (number) Image index

Loads image from the path, supported filetypes are same as supported by
SDL_image [https://wiki.libsdl.org/SDL2_image/FrontPage](https://wiki.libsdl.org/SDL2_image/FrontPage)


### spite-image-draw <a name="spite-image-draw"></a>

Arguments:

- image-index (number)
  - Index returned by spite-image-load
- x (number)
  - Images left top corner x position inside the window
- y (number)
  - Images left top corner y position inside the window
- width (number)
  - What width should the image be drawn
- height (number)
  - What height should the image be drawn


### spite-image-draw-slice <a name="spite-image-draw-slice"></a>

Arguments:

- image-index (number)
  - Index returned by spite-image-load
- x (number)
  - Images left top corner x position inside the window
- y (number)
  - Images left top corner y position inside the window
- width (number)
  - What width should the slice be drawn
- height (number)
  - What height should the slice be drawn
- slice-x (number)
  - Slices top left corner x position inside the image
- slice-y (number)
  - Slices top left corner y position inside the image
- slice-height
  - Slices height
- slice-width
  - Slices width

Slice here refers to rectangle inside the image. So instead of drawing
the whole image, only the slice is drawn


### spite-font-load <a name="spite-font-load"></a>

Arguments:

- path (string)
  - Path to the font file you want to load
- size (number)
  - The size the font should be

Returns: (number) Index of the loaded font

Supports any font format SDL_format supports
[https://wiki.libsdl.org/SDL2_ttf/FrontPage](https://wiki.libsdl.org/SDL2_ttf/FrontPage)


### spite-text-draw <a name="spite-text-draw"></a>

Arguments:

- text (string)
  - Text you want to draw
- x (number)
  - Left top corner x position of the text inside the window
- y (number)
  - Left top corner y position of the text inside the window
- font (number)
  - Font index returned by spite-font-load
- color (vector (number) (number) (number))
  - Vector consisting of three numbers red, green and blue between 0-255


### spite-audio-load <a name="spite-audio-load"></a>

Arguments:

- path (string)
 - Path to the audio file

Returns: (number) Index of loaded audio

Supports any format supported by
[https://www.libsdl.org/projects/old/SDL_mixer/](https://www.libsdl.org/projects/old/SDL_mixer/)


### spite-audio-play <a name="spite-audio-play"></a>

Arguments:

- audio-index (number)
 - Index of the audio returned by spite-audio-load


### spite-start <a name="spite-start"></a>

Arguments:

- title (string)
  - The window title
- widht (number)
  - Width of the window
- height
 - Height of the window


## Events

### key-down

Arguments:

- key (string)
  - The name of the pressed key as a string
  - scancode (number)
    - The scancode of the pressed key
    - repeat? (boolean)
      - If the key press is a repeat (It's hold down)

### key-up

Arguments:

- key (string)
  - The name of the pressed key as a string
  - scancode (number)
    - The scancode of the pressed key
    - repeat? (boolean)
      - If the key press is a repeat (It's hold down)

### mouse-moved

Arguments:

- x (number)
  - X position of the mouse inside the window
  - y (number)
    - Y position of the mouse inside the window

### mouse-button-up

Arguments:

- x (number)
  - X position of the mouse inside the window
  - y (number)
    - Y position of the mouse inside the window
    - button (number)
      - The index of a button that went down
      - clicks
        - The count of clicks, > 1 means doubleclick for example

### mouse-button-down

Arguments:

- x (number)
  - X position of the mouse inside the window
  - y (number)
    - Y position of the mouse inside the window
    - button (number)
      - The index of a button that went down
      - clicks
        - The count of clicks, > 1 means doubleclick for example
