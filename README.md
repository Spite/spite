Game library and such inspired by Love2D built on top of [r7rs-pffi](https://codeberg.org/r7rs-pffi).

Please note that spite is currently in **alpha** stage and not well documented.

[Questions](https://codeberg.org/Spite/spite/projects/9055)

[Bugs](https://codeberg.org/Spite/spite/projects/9056)



spite - [Documentation](https://codeberg.org/Spite/spite/wiki)

[time](https://codeberg.org/Spite/scheme-time) - [Documentation](https://codeberg.org/Spite/scheme-time/wiki/Documentation)


[event](https://codeberg.org/Spite/scheme-event) - [Documentation](https://codeberg.org/Spite/scheme-event/wiki/Documentation)


[geometry](https://codeberg.org/Spite/scheme-geometry) - [Documentation](https://codeberg.org/Spite/scheme-geometry/wiki/Documentation)


