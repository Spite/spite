## Getting Started

### Hello World

Easiest way to get started is with
[spite-hello](https://codeberg.org/Spite/spite-hello).

For windows it contains everything you need, for linux requirements read ahead.

If you want to install spite manually read untill the end.

### System requirements <a name="system-requirements"></a>

#### Linux

##### Sagittarius

On Debian and derivatives (Ubuntu/Mint...) download the latest source from
[https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/](https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/)
unpack it and cd into it, then run

<code>
  apt install cmake libgc-dev zlib1g-dev libffi-dev libssl-dev
  mkdir build
  cd build
  cmake ..
  make
  make install
</code>

##### SDL2

On linux install sdl2 sdl2-image sdl2-ttf sdl2-mixer.

On Debian and derivatives (Ubuntu/Mint...) run

<code>
  apt install libsdl2-2.0-0 libsdl2-image-2.0-0 libsdl2-ttf-2.0-0 libsdl2-mixer-2.0-0
</code>

#### Windows

##### Sagittarius

You can download the installer from here
[https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/](https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/)

##### SDL2

All the needed DLL files are included with Spite.


### Installing

1. Install [Schubert](https://codeberg.org/Schubert)
2. Run "schubert init" in your projects folder
3. Add (retropikzel spite VERSION) into composition.scm dependencies section
4. Run "schubert compose"

Spite is now installed into projects "schubert" folder.

### Manual hello world

Create main.scm with this code

<pre>
<code>
(import (scheme base)
        (scheme write)
        (retropikzel spite v0.1.0 main))
  (spite-load
    (lambda ()
      (display "Loading...")
      (newline)))
  (spite-update
    (lambda ()
      (display "Updating...")
      (newline)))
  (spite-draw
    (lambda ()
      (display "Drawing...")
      (newline)))
  (spite-event-handle
    'key-down
    (lambda (key scancode repeat?)
      (if (string=? key "Space")
      (spite-event-push 'test (list "I am user defined event")))))
  (spite-event-handle
    'test
    (lambda (message)
      (display message)
      (newline)))
  (spite-start "Game Test" 400 400)
</code>
</pre>

Run with either

<code>sash -r7 -L . -L ./schubert main.scm</code>

or

<code>sash.exe -r7 -L . -L ./schubert main.scm</code>

