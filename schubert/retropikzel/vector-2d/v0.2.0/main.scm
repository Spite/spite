;> # vector-2d

(define-library
  (retropikzel vector-2d v0.2.0 main)
  (import (scheme base)
          (scheme write))
  (export vector-2d-make
          vector-2d?
          vector-2d-width-get
          vector-2d-width-set!
          vector-2d-heigth-get
          vector-2d-heigth-set!
          vector-2d-cells-get
          vector-2d-cells-set!
          vector-2d-get
          vector-2d-set!
          vector-2d-display)
  (begin

    ;> ## Records

    ;> ### vector-2d
    ;>
    ;> Fields:
    ;> - width (number)
    ;> - height (number)
    (define-record-type vector-2d
      (vector-2d-make-internal width height cells)
      vector-2d?
      (width vector-2d-width-get vector-2d-width-set!)
      (height vector-2d-height-get vector-2d-height-set!)
      (cells vector-2d-cells-get vector-2d-cells-set!))

    ;> ## Procedures

    ;> ### vector-2d?
    ;>
    ;> Arguments:
    ;> - object (object)
    ;>
    ;> Returns
    ;> - (boolean) Returns #t if object is vector-2d

    ;> ### vector-2d-make
    ;>
    ;> Arguments:
    ;> - width (number)
    ;> - height (number)
    ;> - fill (object) Fill the 2d vector with this value
    (define vector-2d-make
      (lambda (width height fill)
        (let ((cells (make-vector height))
              (index 0))
          (vector-for-each
            (lambda (_)
              (vector-set! cells index (make-vector width fill))
              (set! index (+ index 1)))
            cells)
          (vector-2d-make-internal width height cells))))

    ;> ### vector-2d-get
    ;>
    ;> Arguments:
    ;> - vector-2d (vector-2d)
    ;> - x (number) The x position of the cell
    ;> - y (number) The y position of the cell
    ;>
    ;> Returns
    ;> - (object) The contents of cell in position x y
    (define vector-2d-get
      (lambda (vector-2d x y)
        (vector-ref (vector-ref (vector-2d-cells-get vector-2d) y) x)))

    ;> ### vector-2d-set!
    ;>
    ;> Arguments:
    ;> - vector-2d (vector-2d)
    ;> - x (number) The x position of the cell
    ;> - y (number) The y position of the cell
    ;> - value (object) The new value of the cell
    (define-syntax vector-2d-set!
      (syntax-rules ()
        ((_ vector-2d x y value)
         (vector-set! (vector-ref (vector-2d-cells-get vector-2d) y) x value))))

    ;> ### vector-2d-display
    ;>
    ;> Arguments:
    ;> - vector-2d (vector-2d)
    (define vector-2d-display
      (lambda (vector-2d)
        (vector-for-each
          (lambda (line)
            (display line)
            (newline))
          (vector-2d-cells-get vector-2d))))))
